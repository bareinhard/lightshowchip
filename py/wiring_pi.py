#!/usr/bin/env python
#
# Licensed under the BSD license.  See full license in LICENSE file.
# http://www.lightshowpi.com/
#
# Author: Micah Wedemeyer
# Author: Tom Enos (tomslick.ca@gmail.com)
# Author: Brett Reinhard

"""Empty wrapper module for wiringpi

This module is a place holder for virtual hardware to run a simulated lightshow
an a pc.  This module is not yet functional.
"""
# Useful link for this CHIP port - https://github.com/xtacocorex/CHIP_IO -
# Setup
def wiringPiSetup(*args):
    # Since pinMod achieves this, maybe this can be left blank
    import CHIP_IO.GPIO as GPIO
    #For each statement to setup all pins as outputs
    GPIO.setup("XIO-P0", GPIO.OUT)


def wiringPiSetupSys(*args):
    # Still unsure what to use for this
    pass


def pinMode(*args):
    import CHIP_IO.GPIO as GPIO
    count = 0
    for arg in range (args):
        if count = 0:
            pin = "XIO-P" + str(arg)
            count = count + 1
        else if count = 1:
            mode = arg
    GPIO.setup(pin, mode)
    #I need to learn how to deal with args properly, this should suffice until I learn how


# Pin Writes
def softPwmCreate(*args):
    # wiringpi.softPwmCreate(cm.hardware.gpio_pins[pin], 0, _PWM_MAX)
    import CHIP_IO.SOFTPWM as SPWM
    count = 0
    for arg in range (args):
        if count = 0:
            pin = "XIO-P" + str(arg)
            count = count + 1
        else if count = 1:
            val = arg
            count = count + 1
        else if count = 2:
            _PWM_MAX = arg
            count = count + 1
    SPWM.start(pin,_PWM_MAX)


def softPwmWrite(*args):
    #wiringpi.softPwmWrite(cm.hardware.gpio_pins[pin], int(brightness * _PWM_MAX))
    import CHIP_IO.SOFTPWM as SPWM
    #set duty brightness from args
    #set pin to pin from args
    count = 0
    for arg in range (args):
        if count = 0:
            pin = "XIO-P" + str(arg)
            count = count + 1
        else if count = 1:
            duty = arg
            count = count + 1
    SPMW.start(pin,DUTY)
    SPMW.set_duty_cycle(pin,duty)
    #Not sure what to use for frequency used 100 for default
    SPMW.set_frequency(pin,100)
    


def digitalWrite(*args):
    #wiringpi.digitalWrite(cm.hardware.gpio_pins[pin], int(brightness > 0.5))
    import CHIP_IO.GPIO as GPIO
    count = 0
    for arg in range (args):
        if count = 0:
            pin = "XIO-P" + str(arg)
            count = count + 1
        else if count = 1:
            state = arg
            count = count + 1
    
    GPIO.output(pin,state) #could be GPIO.LOW/HIGH or 1 and 0
    pass


# Devices
def mcp23017Setup(*args):
    pass


def mcp23s17Setup(*args):
    pass


def mcp23016Setup(*args):
    pass


def mcp23008Setup(*args):
    pass


def mcp23s08Setup(*args):
    pass


def sr595Setup(*args):
    pass


def pcf8574Setup(*args):
    pass
